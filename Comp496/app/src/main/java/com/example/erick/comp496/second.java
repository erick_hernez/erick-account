package com.example.erick.comp496;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class second extends AppCompatActivity {
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        OnclickButtonListener();
    }

    public void OnclickButtonListener() {
        btnBack = (Button) findViewById(R.id.bn_back);
        btnBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent returnBtn = new Intent(getApplicationContext(),
                                MainActivity.class);

                        startActivity(returnBtn);
                    }
                });
    }
}