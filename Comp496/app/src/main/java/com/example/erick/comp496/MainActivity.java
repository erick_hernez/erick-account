package com.example.erick.comp496;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {
    Button button1;
    Button button2;
    Button button3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        OnclickButtonListener();
        addListenerOnButton();
        addButtonListener();
    }

    public void OnclickButtonListener() {
        button1 = (Button) findViewById(R.id.buttons2);

        button1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.erick.comp496.second");
                        startActivity(intent);

                    }
                }
        );

    }

    public void addListenerOnButton() {


        button2 = (Button) findViewById(R.id.buttons3);

        button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent("com.example.erick.comp496.third");
                startActivity(intent);

            }

        });
    }

    public void addButtonListener() {


        button3 = (Button) findViewById(R.id.buttons4);

        button3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent("com.example.erick.comp496.fourth");
                startActivity(intent);

            }

        });
    }
}
