package com.example.erick.comp496;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class fourth extends AppCompatActivity {
    Button bnBack2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);
        OnclickButtonListener();
    }

    public void OnclickButtonListener() {
        bnBack2 = (Button) findViewById(R.id.btnBack3);
        bnBack2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent returnBtn = new Intent(getApplicationContext(),
                                MainActivity.class);

                        startActivity(returnBtn);
                    }
                });
    }
}
